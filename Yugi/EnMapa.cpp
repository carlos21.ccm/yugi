#include "Ventana.h"

namespace Yugi
{
	EnMapa::EnMapa()
	{
		Ventana::marco = gcnew Marco(gcnew Posicion(0, 0));
		onTimerTick = gcnew EventHandler(this, &EnMapa::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &EnMapa::teclaDown);
		onKeyUp = gcnew KeyEventHandler(this, &EnMapa::teclaUp);
		onMouseClick = gcnew MouseEventHandler(this, &EnMapa::mouseClick);

		cheatKey = 'z';

		Ventana::marco = gcnew Marco(gcnew Posicion(0, 0));
	}

	void EnMapa::timerTick(System::Object^  sender, System::EventArgs^  e)
	{
		if (activo)
		{
			contador++;
			Mapa::MostrarMapa(buffer->Graphics);
			Ventana::marco->MostrarMarco(buffer->Graphics);
			buffer->Render(Ventana::graphics);
			dibujado = true;
		}
	}

	void EnMapa::teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::W || e->KeyCode == Keys::Up)
			{
				Ventana::marco->moviendose = true;
				Ventana::marco->direccion = Arriba;
			}
			else if (e->KeyCode == Keys::S || e->KeyCode == Keys::Down)
			{
				Ventana::marco->moviendose = true;
				Ventana::marco->direccion = Abajo;
			}
			else if (e->KeyCode == Keys::A || e->KeyCode == Keys::Left)
			{
				Ventana::marco->moviendose = true;
				Ventana::marco->direccion = Izquierda;
			}
			else if (e->KeyCode == Keys::D || e->KeyCode == Keys::Right)
			{
				Ventana::marco->moviendose = true;
				Ventana::marco->direccion = Derecha;
			}
			/*else if (e->KeyCode == Keys::N)
			{
				cheatKey = 'n';
			}*/
			else if (e->KeyCode == Keys::P)
			{
				DesactivarEscena(this);
				//ActivarEscena(Ventana::pausa);
			}
		}
	}

	void EnMapa::teclaUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (e->KeyCode == Keys::W || Ventana::marco->direccion == Arriba)
			Ventana::marco->Detener();
		else if (e->KeyCode == Keys::S || Ventana::marco->direccion == Abajo)
			Ventana::marco->Detener();
		else if (e->KeyCode == Keys::A || Ventana::marco->direccion == Izquierda)
			Ventana::marco->Detener();
		else if (e->KeyCode == Keys::D || Ventana::marco->direccion == Derecha)
			Ventana::marco->Detener();

		
		//if (e->KeyCode == Keys::N && cheatKey == 'n')
			//cheatKey = 'z';
	}

	void EnMapa::mouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		;
	}
}

