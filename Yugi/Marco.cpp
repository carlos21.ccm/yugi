#include "Ventana.h"

namespace Yugi
{
	Marco::Marco(Posicion^ p)
	{
		imagen = Imagenes::YugiSprite;
		indiceSprite = 0;
		direccion = Direcciones::Abajo;
		velocidad = 4;
		posicion = p;
		ancho = 24;
		alto = 24;
		moviendose = false;
	}

	void Marco::MostrarMarco(Graphics^ graphics)
	{
		if (direccion == Arriba)
			subIndice = 0;
		else if (direccion == Abajo)
			subIndice = 1;
		else if (direccion == Izquierda)
			subIndice = 2;
		else if (direccion == Derecha)
			subIndice = 3;

		if (moviendose)
			Avanzar(direccion);

		graphics->DrawImage(imagen, Rectangle(posicion->x, posicion->y, ancho, alto), Rectangle(indiceSprite / 2 * 24, subIndice * 24, 23, 23), GraphicsUnit::Pixel);
	}

	void Marco::Avanzar(Direcciones pDireccion)
	{
		direccion = pDireccion;

		indiceSprite++;

		if (indiceSprite == 3)
			indiceSprite = 4;

		else if (indiceSprite == 6)
			indiceSprite = 0;

		posicion->Aumentar(direccion, velocidad);
	}

	void Marco::Detener()
	{
		indiceSprite = 0;
		moviendose = false;
	}
}