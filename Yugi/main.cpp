#include "Ventana.h"

using namespace System;
using namespace System::Windows::Forms;

namespace Yugi
{
	[STAThread]
	void main()
	{
		Application::EnableVisualStyles();
		Application::SetCompatibleTextRenderingDefault(false);

		Ventana^ w = gcnew Ventana();

		Application::Run(w);
	}
}