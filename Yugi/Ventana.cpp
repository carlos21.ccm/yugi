#include "Ventana.h"

namespace Yugi
{
	Ventana::Ventana(void)
	{
		InitializeComponent();
		ventana = this;

		imagenes = gcnew Imagenes();
		graphics = this->CreateGraphics();
		context = BufferedGraphicsManager::Current;
		aleatorio = gcnew Random();

		enMapa = gcnew EnMapa();

		cambioDeEscena = false;


		//Empezar el juego
		Escena::ActivarEscena(enMapa);
		Mapa::PasarAMapa(1);
	}

	Ventana::~Ventana()
	{
		if (components)
		{
			delete components;
		}
	}

	System::Void Ventana::Ventana_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		;
	}

	System::Void Ventana::Ventana_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		;
	}
}