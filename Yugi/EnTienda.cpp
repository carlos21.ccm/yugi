#include "Ventana.h"

namespace Yugi
{
	EnTienda::EnTienda()
	{

		onTimerTick = gcnew EventHandler(this, &EnTienda::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &EnTienda::teclaDown);
		onKeyUp = gcnew KeyEventHandler(this, &EnTienda::teclaUp);
		onMouseClick = gcnew MouseEventHandler(this, &EnTienda::mouseClick);

		cheatKey = 'z';
	}

	void EnTienda::timerTick(System::Object^  sender, System::EventArgs^  e)
	{
		if (activo)
		{
			contador++;
			//Imagen::Mostrar(Imagen::FondoTienda)
			Mapa::MostrarMapa(buffer->Graphics);
			Ventana::marco->MostrarMarco(buffer->Graphics);
			buffer->Render(Ventana::graphics);
			dibujado = true;
		}
	}

	void EnTienda::teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::W || e->KeyCode == Keys::Up)
			{
				Ventana::marco->moviendose = true;
				Ventana::marco->direccion = Arriba;
			}
			else if (e->KeyCode == Keys::S || e->KeyCode == Keys::Down)
			{
				Ventana::marco->moviendose = true;
				Ventana::marco->direccion = Abajo;
			}
			else if (e->KeyCode == Keys::A || e->KeyCode == Keys::Left)
			{
				Ventana::marco->moviendose = true;
				Ventana::marco->direccion = Izquierda;
			}
			else if (e->KeyCode == Keys::D || e->KeyCode == Keys::Right)
			{
				Ventana::marco->moviendose = true;
				Ventana::marco->direccion = Derecha;
			}
			/*else if (e->KeyCode == Keys::N)
			{
			cheatKey = 'n';
			}*/
			else if (e->KeyCode == Keys::P)
			{
				DesactivarEscena(this);
				//ActivarEscena(Ventana::pausa);
			}
		}
	}

	void EnTienda::teclaUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (e->KeyCode == Keys::W || Ventana::marco->direccion == Arriba)
			Ventana::marco->Detener();
		else if (e->KeyCode == Keys::S || Ventana::marco->direccion == Abajo)
			Ventana::marco->Detener();
		else if (e->KeyCode == Keys::A || Ventana::marco->direccion == Izquierda)
			Ventana::marco->Detener();
		else if (e->KeyCode == Keys::D || Ventana::marco->direccion == Derecha)
			Ventana::marco->Detener();


		//if (e->KeyCode == Keys::N && cheatKey == 'n')
		//cheatKey = 'z';
	}

	void EnTienda::mouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		;
	}
}

