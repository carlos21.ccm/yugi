#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace Yugi
{
	public enum Direcciones { Arriba, Abajo, Izquierda, Derecha };
	public enum TipoCarta { A, B, C, D };
	public enum Pertenencia { Tienda, DeMarco, DeProfesor };
	public enum ModoBatalla { Defensa, Ataque };
	public enum Malignos { m1, m2, m3, m4 };
	public enum Objetos { eExtra, ePared, eSuelo, eDoors, eMoney };

	public ref class Posicion
	{
	public:
		int x;
		int y;
		Posicion(int pX, int pY);
		Posicion(int pX, int pY, int multiplo);
		bool EsIgualA(Posicion^ p);
		void Igualar(Posicion^ p);
		void Igualar(int pX, int pY, int multiplo);
		void Aumentar(Direcciones direccion, int velocidad);
		void ToZero();
		Rectangle getBody(int pAncho, int pAlto);
		Posicion^ getIncrementada(Direcciones direccion, int velocidad);
	};

	public ref class Figura
	{
	public:
		Image^ imagen;
		Posicion^ posicion;
		int alto;
		int ancho;
		int indiceSprite;
		int subIndice;
		Figura();
		Rectangle getBody();
		static Rectangle getBody(int xx, int yy, int pAncho, int pAlto);
	};

	public ref class Carta : Figura
	{
	public:
		TipoCarta tipo;
		Pertenencia pertenencia;
		int ataque;
		int defensa;
		int nivel;
		Carta();
		void MostrarCarta(Graphics^ graphics);
	};

	public ref class  Baraja 
	{
	public:
		Pertenencia pertenencia;
		Baraja();
	};

	public ref class Marco : public Figura
	{
	public:
		static bool moviendose;
		Direcciones direccion;
		int velocidad;
		Marco(Posicion^ p);
		void MostrarMarco(Graphics^ graphics);
		void Avanzar(Direcciones direccion);
		void Detener();
	};

	public ref class Escena
	{
	public:
		int contador;
		bool activo;
		bool dibujado;
		BufferedGraphics^ buffer;
		KeyEventHandler^ onKeyDown;
		KeyEventHandler^ onKeyUp;
		MouseEventHandler^ onMouseClick;
		EventHandler^ onTimerTick;
		Escena();
		static void CambiarEscena(Escena^ escena);
		static void ActivarEscena(Escena^ escena);
		static void DesactivarEscena(Escena^ escena);
	};

	public ref class EnMapa : public Escena
	{
	public:
		int mapa;
		EnMapa();
		char cheatKey;
		void mouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
		void teclaUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	};

	public ref class EnTienda : public Escena
	{
	public:
		int mapa;
		EnTienda();
		char cheatKey;
		void mouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
		void teclaUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	};

	public ref class Imagenes
	{
	public:
		Imagenes();
		static Image^ YugiSprite;
		static Image^ Suelo1;
		static Image^ Pared1;
		static Image^ Money1;
		static Image^ Doors1;
		static Image^ Extra1;
	};

	public ref class Objeto : public Figura
	{
	public:
		Objetos tipo;
		static Objeto^ getObjeto(Posicion^ posicion);
	};
	/*Extra, Pared, Suelo, Doors, Money*/

	public ref class Extra : public Objeto
	{
	public:
		Extra(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};

	public ref class Pared : public Objeto
	{
	public:
		Pared(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};

	public ref class Suelo : public Objeto
	{
	public:
		Suelo(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};

	public ref class Doors : public Objeto
	{
	public:
		Doors(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};

	public ref class Money : public Objeto
	{
	public:
		Money(Posicion^ p);
		void MostrarSprite(Graphics^ graphics);
	};

	public ref class MatrizObjetos
	{
	public:
		array<Objeto^, 2>^ matriz;
	};

	public ref class Mapa
	{
	public:
		MatrizObjetos^ matrizObjetos;
		static void PasarAMapa(int pMapa);
		void generarMatrizObjetos(Objetos matriz[25][25]);
		static void MostrarMapa(Graphics^ graphics);
	};

	public ref class Mapa1 : public Mapa{ public: Mapa1(); };
	public ref class Mapa2 : public Mapa{ public: Mapa2(); };
	//public ref class Mapa3 : public Mapa{ public: Mapa3(); };
	//public ref class Mapa4 : public Mapa{ public: Mapa4(); };

}