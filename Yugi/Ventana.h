#include "Directorio.h"
#pragma once

namespace Yugi {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Ventana
	/// </summary>
	public ref class Ventana : public System::Windows::Forms::Form
	{
	public:
		static Ventana^ ventana;
		static Imagenes^ imagenes;
		static Graphics^ graphics;
		static BufferedGraphicsContext^ context;
		static Random^ aleatorio;
		static Marco^ marco;
		static MatrizObjetos^ objetos;

		static EnMapa^ enMapa;

		static Mapa1^ mapa1;
		static Mapa2^ mapa2;

		bool cambioDeEscena;

		Ventana(void);
		~Ventana();
	public: System::Windows::Forms::Timer^  timer;


	private: System::ComponentModel::IContainer^  components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 this->components = (gcnew System::ComponentModel::Container());
				 this->timer = (gcnew System::Windows::Forms::Timer(this->components));
				 this->SuspendLayout();
				 // 
				 // timer
				 // 
				 this->timer->Enabled = true;
				 this->timer->Interval = 50;
				 // 
				 // Ventana
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(600, 600);
				 this->MaximumSize = System::Drawing::Size(616, 639);
				 this->MinimumSize = System::Drawing::Size(616, 639);
				 this->Name = L"Ventana";
				 this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 this->Text = L"Yu-Gi-Oh!";
				 this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Ventana::Ventana_KeyDown);
				 this->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &Ventana::Ventana_KeyUp);
				 this->ResumeLayout(false);

			 }
#pragma endregion

	private: System::Void Ventana_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);

	private: System::Void Ventana_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);

	};
}

