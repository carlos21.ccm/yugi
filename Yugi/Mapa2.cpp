#include "Ventana.h"

namespace Yugi
{
	Mapa2::Mapa2()
	{
		Objetos matriz[25][25] = {
			{ ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eMoney, eMoney, eMoney, eMoney, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eMoney, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, ePared, eMoney, eMoney, eMoney, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, ePared, eMoney, eMoney, eMoney, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eDoors },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eMoney, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eMoney, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared, ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared } };


		generarMatrizObjetos(matriz);
	}
}