#include "Ventana.h"

namespace Yugi
{
	void Mapa::MostrarMapa(Graphics^ graphics)
	{
		for (int x = 0; x < 25; x++)
		{
			for (int y = 0; y < 25; y++)
			{
				/*Extra, Pared, Suelo, Doors, Money*/
				if (Ventana::objetos->matriz[x, y]->tipo == eExtra)
					dynamic_cast<Extra^>(Ventana::objetos->matriz[x, y])->MostrarSprite(graphics);
				else if (Ventana::objetos->matriz[x, y]->tipo == ePared)
					dynamic_cast<Pared^>(Ventana::objetos->matriz[x, y])->MostrarSprite(graphics);
				else if (Ventana::objetos->matriz[x, y]->tipo == eSuelo)
					dynamic_cast<Suelo^>(Ventana::objetos->matriz[x, y])->MostrarSprite(graphics);
				else if (Ventana::objetos->matriz[x, y]->tipo == eDoors)
					dynamic_cast<Doors^>(Ventana::objetos->matriz[x, y])->MostrarSprite(graphics);
				else if (Ventana::objetos->matriz[x, y]->tipo == eMoney)
					dynamic_cast<Money^>(Ventana::objetos->matriz[x, y])->MostrarSprite(graphics);
			}
		}
	}

	Objeto^ Objeto::getObjeto(Posicion^ posicion)
	{
		return Ventana::objetos->matriz[posicion->x / 64, posicion->y / 64];
	}

	void Mapa::generarMatrizObjetos(Objetos matriz[25][25])
	{
		matrizObjetos = gcnew MatrizObjetos();
		matrizObjetos->matriz = gcnew array<Objeto^, 2>(25, 25);

		for (int y = 0; y < 25; y++)
		{
			for (int x = 0; x < 25; x++)
			{/*Extra, Pared, Suelo, Doors, Money*/
				if (matriz[y][x] == eSuelo)
					matrizObjetos->matriz[x, y] = gcnew Suelo(gcnew Posicion(x * 24, y * 24));
				else if (matriz[y][x] == ePared)
					matrizObjetos->matriz[x, y] = gcnew Pared(gcnew Posicion(x * 24, y * 24));
				else if (matriz[y][x] == eMoney)
					matrizObjetos->matriz[x, y] = gcnew Money(gcnew Posicion(x * 24, y * 24));
				else if (matriz[y][x] == eDoors)
					matrizObjetos->matriz[x, y] = gcnew Doors(gcnew Posicion(x * 24, y * 24));
			}
		}
	}

	void Mapa::PasarAMapa(int pMapa)
	{
		Ventana::enMapa->mapa = pMapa;

		if (pMapa == 1)
		{
			Ventana::mapa1 = gcnew Mapa1();
			Ventana::objetos = Ventana::mapa1->matrizObjetos;
		}

		else if (pMapa == 2)
		{
			Ventana::mapa2 = gcnew Mapa2();
			Ventana::objetos = Ventana::mapa2->matrizObjetos;
		}


		Ventana::ventana->marco->indiceSprite = 0;
		Ventana::ventana->marco->direccion = Direcciones::Abajo;
		Ventana::ventana->marco->moviendose = false;
		Ventana::ventana->marco->posicion->Igualar(gcnew Posicion(2,2));
	}

}