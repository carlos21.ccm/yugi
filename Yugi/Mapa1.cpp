#include "Ventana.h"

namespace Yugi
{
	Mapa1::Mapa1()
	{
		Objetos matriz[25][25] = {
			{ ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, eDoors, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eMoney, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eDoors },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eMoney, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eMoney, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, eSuelo, ePared },
			{ ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared, ePared } };


		generarMatrizObjetos(matriz);
	}
}