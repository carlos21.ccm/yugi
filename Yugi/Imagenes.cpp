#include "Ventana.h"

namespace Yugi
{
	Imagenes::Imagenes()
	{
		//Personaje
		Imagenes::YugiSprite = Image::FromFile("Imagenes\\Personajes\\Yugi.png");

		//Objetos
		Imagenes::Suelo1 = Image::FromFile("Imagenes\\Objetos\\Piso1.png");
		Imagenes::Pared1 = Image::FromFile("Imagenes\\Objetos\\Bloque1.png");
		Imagenes::Money1 = Image::FromFile("Imagenes\\Objetos\\Caja1.png");
		Imagenes::Doors1 = Image::FromFile("Imagenes\\Objetos\\Portal.png");
		Imagenes::Extra1 = Image::FromFile("Imagenes\\Objetos\\Portal.png");
	}
}