#include "Ventana.h"

namespace Yugi
{
	Escena::Escena()
	{
		buffer = Ventana::context->Allocate(Ventana::graphics, Ventana::ventana->ClientRectangle);
		activo = false;
		dibujado = false;
		contador = 0;
	}

	void Escena::CambiarEscena(Escena^ escena)
	{
		if (Ventana::enMapa->activo)
			DesactivarEscena(Ventana::enMapa);

		ActivarEscena(escena);
	}

	void Escena::ActivarEscena(Escena^ escena)
	{
		escena->activo = true;

		if (escena->onTimerTick != nullptr)
			Ventana::ventana->timer->Tick += escena->onTimerTick;
		if (escena->onTimerTick != nullptr)
			Ventana::ventana->KeyDown += escena->onKeyDown;
		if (escena->onTimerTick != nullptr)
			Ventana::ventana->KeyUp += escena->onKeyUp;
		if (escena->onMouseClick != nullptr)
			Ventana::ventana->MouseClick += escena->onMouseClick;
	}

	void Escena::DesactivarEscena(Escena^ escena)
	{
		escena->activo = false;

		if (escena->onTimerTick != nullptr)
			Ventana::ventana->timer->Tick -= escena->onTimerTick;
		if (escena->onTimerTick != nullptr)
			Ventana::ventana->KeyDown -= escena->onKeyDown;
		if (escena->onTimerTick != nullptr)
			Ventana::ventana->KeyUp -= escena->onKeyUp;
		if (escena->onMouseClick != nullptr)
			Ventana::ventana->MouseClick -= escena->onMouseClick;

		escena->dibujado = false;
	}
}